//
//  DSRStylishAllowableItemsProtocol.h
//
//
//  Created by Zamogilin Andrey on 08.04.16.
//
//

#import <Foundation/Foundation.h>
#import "DSRStylishItem.h"

@protocol DSRStylishAllowableItemsProtocol <NSObject>

+ (BOOL)keyIsAllow:(DSRStylishItemKey)key;
+ (BOOL)stateIsAllow:(DSRStylishItemState)state;
+ (NSString *)styleProviderForKey:(DSRStylishItemKey)key;

@end