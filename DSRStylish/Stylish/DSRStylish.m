//
//  DSRStylish.m
//  DSRCloudMobileClient
//
//  Created by Zamogilin Andrey on 08.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import "DSRStylish.h"
#import "DSRStylishItem.h"
#import "DSRStylishStyleClass.h"
#import "DSRStylishStyleClassRepository.h"
#import "DSRStylishProviderProtocol.h"
#import "DSRStylishAllowableItemsProtocol.h"

static NSString *const kImportStyleKey = @"import-style";
static NSString *const kDSRStylishParentKey = @"__parent_class";

@interface DSRStylish ()

@property (strong, nonatomic) NSMutableDictionary *styles;
@property (strong, nonatomic) NSMutableDictionary *classes;
@property (strong, nonatomic) DSRStylishStyleClassRepository *classRepository;

@end

@implementation DSRStylish

+ (DSRStylish *)defaultStylish
{
    static DSRStylish *sharedManager = nil;

    if (!sharedManager)
    {
        sharedManager = [[DSRStylish alloc] initWithFilename:@"default-style" recursive:NO];
    }

    return sharedManager;
}

- (instancetype)initWithFilename:(NSString *)filename recursive:(BOOL)recursive
{
    self = [super init];

    if (self)
    {
        self.styles =
            [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:filename ofType:@"plist"]] mutableCopy];

        if (!self.styles)
        {
            [self testLoadFromOtherBundlesWithFilename:filename];

            if (!self.styles)
            {
                NSAssert(NO, @"Can not find styles file");
            }
        }

        self.classes = [NSMutableDictionary dictionary];
        [self expandStylesWithDict:[self.styles objectForKey:@"classes"] recursive:YES className:nil parentClassName:nil state:nil];

        if (recursive)
        {
            [self loadOtherFiles];
        }
    }

    return self;
}

- (void)testLoadFromOtherBundlesWithFilename:(NSString *)filename
{
    NSArray *bundles = [NSBundle allBundles];

    for (NSBundle *bundle in bundles)
    {
        self.styles = [[NSDictionary dictionaryWithContentsOfFile:[bundle pathForResource:filename ofType:@"plist"]] mutableCopy];

        if (self.styles)
        {
            break;
        }
    }
}

- (void)expandStylesWithDict:(NSDictionary *)dict
                   recursive:(BOOL)recursive
                   className:(NSString *)className
             parentClassName:(NSString *)parentClassName
                       state:(NSString *)state
{

    for (NSString *keyString in dict)
    {
        id value = [dict objectForKey:keyString];

        if ([[keyString substringToIndex:1] isEqualToString:@"."] && [value isKindOfClass:[NSDictionary class]])
        {
            NSString *newClassName = keyString;

            if (className)
            {
                newClassName = [NSString stringWithFormat:@"%@%@", className, keyString];
            }

            [self expandStylesWithDict:value recursive:YES className:newClassName parentClassName:className state:nil];
        }
        else if ([[keyString substringToIndex:2] isEqualToString:@"__"] && [value isKindOfClass:[NSDictionary class]])
        {
            [self expandStylesWithDict:value recursive:YES className:className parentClassName:nil state:keyString];
        }
        else
        {
            DSRStylishItemKey key = [DSRStylishItem keyFromString:keyString];
            if ([DSRStylishItem allowKey:key forClass:nil] && className)
            {
                NSMutableDictionary *temp = [NSMutableDictionary dictionary];
                if ([self.classes objectForKey:className] && [[self.classes objectForKey:className] isKindOfClass:[NSDictionary class]])
                {
                    temp = [[self.classes objectForKey:className] mutableCopy];
                }

                [temp setObject:value forKey:keyString];

                if (parentClassName)
                {
                    [temp setObject:parentClassName forKey:kDSRStylishParentKey];
                }

                NSString *stateString = @"normal";

                if (state)
                {
                    stateString = [state substringFromIndex:2];
                }

                NSMutableDictionary *stateList = [[self.classes objectForKey:className] mutableCopy];

                if (!stateList)
                {
                    stateList = [NSMutableDictionary dictionary];
                }

                NSMutableDictionary *concreteState = [[stateList objectForKey:stateString] mutableCopy];

                if (!concreteState)
                {
                    concreteState = [NSMutableDictionary dictionary];
                }

                [concreteState setObject:value forKey:keyString];
                [stateList setObject:concreteState forKey:stateString];

                [self.classes setObject:stateList forKey:className];
            }
        }
    }

    [self expandParentProperties];
}

- (void)expandParentProperties
{
    self.classRepository = [[DSRStylishStyleClassRepository alloc] init];
    for (NSString *name in self.classes)
    {
        DSRStylishStyleClass *styleClass = [self.classRepository findOrCreateWithFullName:name];
        NSDictionary *stateList = [self.classes objectForKey:name];

        for (NSString *stateName in stateList)
        {
            NSDictionary *keysValues = [stateList objectForKey:stateName];

            for (NSString *keyString in keysValues)
            {
                id value = [keysValues objectForKey:keyString];

                if ([keyString isEqualToString:kDSRStylishParentKey])
                {
                    DSRStylishStyleClass *parentStyleClass = [self.classRepository findOrCreateWithFullName:value];
                    styleClass.parent = parentStyleClass;
                }

                DSRStylishItemKey key = [DSRStylishItem keyFromString:keyString];
                DSRStylishItemState state = [DSRStylishItem stateFromString:stateName];

                if ([DSRStylishItem allowKey:key forClass:nil])
                {
                    [styleClass addKey:key withValue:value state:state];
                }
            }
        }
    }
}

- (void)loadOtherFiles
{
    //    if ([self.styles objectForKey:kImportStyleKey])
    //    {
    //        NSArray *files = [self.styles objectForKey:kImportStyleKey];
    //        for (NSString *filename in files)
    //        {
    //            // NSString *pathForResource = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
    //            // TODO: load from other files
    //        }
    //    }
}

- (void)applyStyleWithItem:(DSRStylishItem *)item forView:(UIView<DSRStylishAllowableItemsProtocol> *)view;
{

    NSString *errorMessage = [NSString
        stringWithFormat:@"Incorrect parameter :%@ for class :%@", [DSRStylishItem keyStringFromKey:item.key], [[view class] description]];
    NSAssert([DSRStylishItem allowKey:item.key forClass:[view class]], errorMessage);

    NSString *errorMessage2 = [NSString stringWithFormat:@"Incorrect state :%@ for class :%@",
                                                         [DSRStylishItem stateStringFromState:item.state], [[view class] description]];
    NSAssert([DSRStylishItem allowState:item.state forClass:[view class]], errorMessage2);

    NSString *providerClassString = [[view class] styleProviderForKey:item.key];
    Class<DSRStylishProviderProtocol> providerClass = NSClassFromString(providerClassString);

    UIControlState state = [DSRStylishItem uistateFromState:item.state];
    [providerClass applyParameter:item.value toView:view withState:state];
}

- (DSRStylishStyleClass *)loadStyleClassItems:(NSString *)styleClass
{
    NSString *styleClassCorrected = [NSString stringWithFormat:@".%@", [styleClass lowercaseString]];
    return [self.classRepository findClassByFullName:styleClassCorrected];
}

@end