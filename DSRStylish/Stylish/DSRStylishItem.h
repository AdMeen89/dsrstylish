//
//  DSRStylishItem.h
//
//
//  Created by Zamogilin Andrey on 08.04.16.
//
//

/*
 How support new item.

 1. add item in DSRStylishItemKey enum
 2. add text representation in keyStringFromKey: method
 3. create provider class (see DSRStylishProviders.h)
 4. support provider in UIView+DSRStylish category, method styleProviderForKey:
 5. done!
 */

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DSRStylishItemKey) {
    DSRStylishItemKeyBorderColor,
    DSRStylishItemKeyBorderWidth,
    DSRStylishItemKeyBorderCornerRadius,
    
    DSRStylishItemKeyBackgroundColor,
    DSRStylishItemKeyOpacity,
    DSRStylishItemKeyHidden,
    DSRStylishItemKeyClipsToBounds,
    DSRStylishItemKeyTransform,
    
    
    DSRStylishItemKeyColor,
    DSRStylishItemKeyAlignment,
    DSRStylishItemKeyText,

    DSRStylishItemKeyFontSize,
    DSRStylishItemKeyFontFamily,

    // segmented-control only
    DSRStylishItemKeyTitles,
    DSRStylishItemKeyDividerColor,

    // this item ALWAYS should be in end of list (DSRStylishItemKeyUnknown)
    DSRStylishItemKeyUnknown
};

typedef NS_ENUM(NSInteger, DSRStylishItemState) {
    DSRStylishItemStateSelected,
    DSRStylishItemStateHighlighed,
    DSRStylishItemStateDisabled,

    // this item ALWAYS should be in end of list (DSRStylishItemStateNormal)
    DSRStylishItemStateNormal
};

@interface DSRStylishItem : NSObject

@property (nonatomic, readonly) DSRStylishItemKey key;
@property (nonatomic, readonly) DSRStylishItemState state;
@property (nonatomic, copy, readonly) NSString *value;

+ (DSRStylishItem *)itemWithKey:(DSRStylishItemKey)key value:(id)value state:(DSRStylishItemState)state;

+ (BOOL)allowKey:(DSRStylishItemKey)key forClass:(Class)c_class;
+ (BOOL)allowState:(DSRStylishItemState)state forClass:(Class)c_class;

+ (NSString *)keyStringFromKey:(DSRStylishItemKey)key;
+ (DSRStylishItemKey)keyFromString:(NSString *)string;

+ (NSString *)stateStringFromState:(DSRStylishItemState)state;
+ (DSRStylishItemState)stateFromString:(NSString *)string;

+ (UIControlState)uistateFromState:(DSRStylishItemState)state;

@end