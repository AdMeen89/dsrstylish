//
//  DSRStylishStyleClass.h
//  DSRCloudMobileClient
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DSRStylishItem.h"

@interface DSRStylishStyleClass : NSObject

@property (copy, nonatomic, readonly) NSString *fullname;
@property (strong, nonatomic) DSRStylishStyleClass *parent;
@property (strong, nonatomic, readonly) NSArray *allStates;

@property (nonatomic, readonly) NSInteger count;

- (DSRStylishStyleClass *)initWithName:(NSString *)name;
- (void)addKey:(DSRStylishItemKey)key withValue:(id)value state:(DSRStylishItemState)state;
- (NSArray *)allItems;

@end