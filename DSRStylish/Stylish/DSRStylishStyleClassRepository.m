//
//  DSRStylishStyleClassRepository.m
//  DSRCloudMobileClient
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import "DSRStylishStyleClassRepository.h"
#import "DSRStylishStyleClass.h"

@interface DSRStylishStyleClassRepository ()

@property (strong, nonatomic) NSMutableDictionary *items;

@end

@implementation DSRStylishStyleClassRepository

- (instancetype)init
{
    self = [super init];

    if (self)
    {
        self.items = [NSMutableDictionary dictionary];
    }

    return self;
}

- (DSRStylishStyleClass *)findClassByFullName:(NSString *)name
{
    return [self.items objectForKey:name];
}

- (DSRStylishStyleClass *)findOrCreateWithFullName:(NSString *)name
{
    DSRStylishStyleClass *styleClass = [self findClassByFullName:name];

    if (!styleClass)
    {
        styleClass = [[DSRStylishStyleClass alloc] initWithName:name];
    }

    [self.items setObject:styleClass forKey:styleClass.fullname];

    return styleClass;
}

@end