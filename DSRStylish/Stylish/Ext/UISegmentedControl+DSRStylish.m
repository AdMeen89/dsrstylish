//
//  UISegmentedControl+DSRStylish.m
//  DSRStylish
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import "UISegmentedControl+DSRStylish.h"
#import "DSRStylishItem.h"
#import "UIColor+DSRStylish.h"
#import "NSString+DSRStylish.h"

@implementation UISegmentedControl (DSRStylish)

+ (BOOL)keyIsAllow:(DSRStylishItemKey)key
{
    if ([super keyIsAllow:key])
    {
        return YES;
    }

    switch (key)
    {
    case DSRStylishItemKeyTitles:
    case DSRStylishItemKeyFontFamily:
    case DSRStylishItemKeyFontSize:
    case DSRStylishItemKeyColor:
    case DSRStylishItemKeyDividerColor:
        return YES;

    default:
        return NO;
    }
}

+ (BOOL)stateIsAllow:(DSRStylishItemState)state
{
    switch (state)
    {
    case DSRStylishItemStateNormal:
    case DSRStylishItemStateSelected:
    case DSRStylishItemStateDisabled:
    case DSRStylishItemStateHighlighed:
        return YES;
    }
}

@end