//
//  UISegmentedControl+DSRStylish.h
//  DSRStylish
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+DSRStylish.h"

@interface UISegmentedControl (DSRStylish)

@end