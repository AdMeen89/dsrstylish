//
//  UIView+DSRStylish.m
//  DSRCloudMobileClient
//
//  Created by Zamogilin Andrey on 08.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import "UIView+DSRStylish.h"
#import "DSRStylish.h"
#import "UIColor+DSRStylish.h"
#import "DSRStylishItem.h"
#import "DSRStylishStyleClass.h"
#import "DSRStylishProviders.h"
#import <objc/runtime.h>

static void *kClassStylePointer = &kClassStylePointer;

@implementation UIView (DSRStylish)

- (void)updateStyle
{
    if (self.styleClass == nil)
    {
        return;
    }

    if ([self.styleClass isKindOfClass:[NSString class]])
    {

        DSRStylishStyleClass *styleItems = [[DSRStylish defaultStylish] loadStyleClassItems:self.styleClass];

        for (NSInteger i = 0; i < styleItems.count; i++)
        {
            DSRStylishItem *item = [styleItems.allItems objectAtIndex:i];
            [[DSRStylish defaultStylish] applyStyleWithItem:item forView:self];
        }
    }
}

#pragma mark - DSRStylishAllowableItemsProtocol

+ (NSString *)styleProviderForKey:(DSRStylishItemKey)key
{
    switch (key)
    {
    case DSRStylishItemKeyText:
        return [[DSRStylishTextProvider class] description];

    case DSRStylishItemKeyColor:
        return [[DSRStylishColorProvider class] description];

    case DSRStylishItemKeyHidden:
        return [[DSRStylishHiddenProvider class] description];

    case DSRStylishItemKeyOpacity:
        return [[DSRStylishOpacityProvider class] description];

    case DSRStylishItemKeyAlignment:
        return [[DSRStylishAlignmentProvider class] description];

    case DSRStylishItemKeyBorderColor:
        return [[DSRStylishBorderColorProvider class] description];

    case DSRStylishItemKeyBorderWidth:
        return [[DSRStylishBorderWidthProvider class] description];

    case DSRStylishItemKeyClipsToBounds:
        return [[DSRStylishClipsToBoundsProvider class] description];

    case DSRStylishItemKeyBackgroundColor:
        return [[DSRStylishBackgroundColorProvider class] description];

    case DSRStylishItemKeyBorderCornerRadius:
        return [[DSRStylishBorderCornerRadiusProvider class] description];

    case DSRStylishItemKeyFontSize:
        return [[DSRStylishFontSizeProvider class] description];

    case DSRStylishItemKeyFontFamily:
        return [[DSRStylishFontFamilyProvider class] description];

    case DSRStylishItemKeyTitles:
        return [[DSRStylishTitlesProvider class] description];

    case DSRStylishItemKeyDividerColor:
        return [[DSRStylishDividerProvider class] description];

    case DSRStylishItemKeyTransform:
        return [[DSRStylishTransformProvider class] description];

    case DSRStylishItemKeyUnknown:
        return [[DSRStylishDefaultProvider class] description];
    }
}

+ (BOOL)keyIsAllow:(DSRStylishItemKey)key
{
    switch (key)
    {
    case DSRStylishItemKeyOpacity:
    case DSRStylishItemKeyHidden:
    case DSRStylishItemKeyBorderColor:
    case DSRStylishItemKeyBorderWidth:
    case DSRStylishItemKeyClipsToBounds:
    case DSRStylishItemKeyBackgroundColor:
    case DSRStylishItemKeyBorderCornerRadius:
    case DSRStylishItemKeyUnknown:
    case DSRStylishItemKeyTransform:
        return YES;

    default:
        return NO;
    }
}

+ (BOOL)stateIsAllow:(DSRStylishItemState)state
{
    switch (state)
    {
    case DSRStylishItemStateNormal:
        return YES;

    default:
        return NO;
    }
}

#pragma mark - runtime

- (void)setStyleClass:(id)classStyle
{
    NSAssert([classStyle isKindOfClass:[NSArray class]] || [classStyle isKindOfClass:[NSString class]],
             @"classStyle parameter should be kind of class Array or String");
    objc_setAssociatedObject(self, kClassStylePointer, classStyle, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self updateStyle];
}

- (id)styleClass
{
    return objc_getAssociatedObject(self, kClassStylePointer);
}

@end