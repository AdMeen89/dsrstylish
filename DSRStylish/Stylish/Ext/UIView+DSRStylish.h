//
//  UIView+DSRStylish.h
//  DSRCloudMobileClient
//
//  Created by Zamogilin Andrey on 08.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSRStylishAllowableItemsProtocol.h"

@class DSRStylishItem;

@interface UIView (DSRStylish) <DSRStylishAllowableItemsProtocol>
// should be kind of nsarray or nsstring
@property (nonatomic, strong) id styleClass;

@end