//
//  UILabel+DSRStylish.m
//  DSRCloudMobileClient
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import "UILabel+DSRStylish.h"
#import "DSRStylishItem.h"
#import "UIColor+DSRStylish.h"
#import "NSString+DSRStylish.h"

@implementation UILabel (DSRStylish)

+ (BOOL)keyIsAllow:(DSRStylishItemKey)key
{
    if ([super keyIsAllow:key])
    {
        return YES;
    }

    switch (key)
    {
    case DSRStylishItemKeyAlignment:
    case DSRStylishItemKeyText:
    case DSRStylishItemKeyFontFamily:
    case DSRStylishItemKeyFontSize:
    case DSRStylishItemKeyColor:
        return YES;

    default:
        return NO;
    }
}

@end