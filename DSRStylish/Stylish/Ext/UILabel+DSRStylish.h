//
//  UILabel+DSRStylish.h
//  DSRCloudMobileClient
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+DSRStylish.h"

@interface UILabel (DSRStylish)

@end