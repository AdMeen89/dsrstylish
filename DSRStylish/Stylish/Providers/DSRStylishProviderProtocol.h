//
//  DSRStylishProviderProtocol.h
//  DSRStylish
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DSRStylishProviderProtocol <NSObject>

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state;

@end