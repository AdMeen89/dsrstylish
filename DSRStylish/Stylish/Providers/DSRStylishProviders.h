//
//  DSRStylishProviders.h
//  DSRStylish
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DSRStylishProviderProtocol.h"

#pragma mark - DSRStylishItemKeyBorderColor

@interface DSRStylishBorderColorProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyBorderWidth

@interface DSRStylishBorderWidthProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyBorderCornerRadius

@interface DSRStylishBorderCornerRadiusProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyBackgroundColor

@interface DSRStylishBackgroundColorProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyOpacity

@interface DSRStylishOpacityProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyHidden

@interface DSRStylishHiddenProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyClipsToBounds

@interface DSRStylishClipsToBoundsProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyColor

@interface DSRStylishColorProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyAlignment

@interface DSRStylishAlignmentProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyText

@interface DSRStylishTextProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyFontSize

@interface DSRStylishFontSizeProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyFontFamily

@interface DSRStylishFontFamilyProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyTitles

@interface DSRStylishTitlesProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyDividerColor

@interface DSRStylishDividerProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyTransform

@interface DSRStylishTransformProvider : NSObject <DSRStylishProviderProtocol>
@end

#pragma mark - DSRStylishItemKeyUnknown

@interface DSRStylishDefaultProvider : NSObject <DSRStylishProviderProtocol>
@end