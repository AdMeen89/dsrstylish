//
//  DSRStylishProviders.m
//  DSRStylish
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import "DSRStylishProviders.h"
#import "UIColor+DSRStylish.h"

#pragma mark - DSRStylishItemKeyBorderColor

@implementation DSRStylishBorderColorProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    view.layer.borderColor = [[UIColor colorWithHexColor:value] CGColor];
}

@end

#pragma mark - DSRStylishItemKeyBorderWidth

@implementation DSRStylishBorderWidthProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    view.layer.borderWidth = [value floatValue];
}

@end

#pragma mark - DSRStylishItemKeyBorderCornerRadius

@implementation DSRStylishBorderCornerRadiusProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    view.layer.cornerRadius = [value floatValue];
}

@end

#pragma mark - DSRStylishItemKeyBackgroundColor

@implementation DSRStylishBackgroundColorProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    view.backgroundColor = [UIColor colorWithColorString:value];

    if ([view isKindOfClass:[UISegmentedControl class]])
    {
        UIColor *color = [UIColor colorWithColorString:value];
        UIImage *backgroundImage = [UIColor imageFromColor:color];
        UISegmentedControl *seg = (UISegmentedControl *)view;
        [seg setBackgroundImage:backgroundImage forState:state barMetrics:UIBarMetricsDefault];
    }
}

@end

#pragma mark - DSRStylishItemKeyOpacity

@implementation DSRStylishOpacityProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    view.alpha = [value floatValue];
}

@end

#pragma mark - DSRStylishItemKeyHidden

@implementation DSRStylishHiddenProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    view.hidden = [value boolValue];
}

@end

#pragma mark - DSRStylishItemKeyClipsToBounds

@implementation DSRStylishClipsToBoundsProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    view.clipsToBounds = [value boolValue];
}

@end

#pragma mark - DSRStylishItemKeyColor

@implementation DSRStylishColorProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    if ([view isKindOfClass:[UISegmentedControl class]])
    {
        UISegmentedControl *seg = (UISegmentedControl *)view;
        NSMutableDictionary *textAttr = [[seg titleTextAttributesForState:state] mutableCopy];
        UIColor *color = [UIColor colorWithColorString:value];
        [textAttr setObject:color forKey:NSForegroundColorAttributeName];
        [seg setTitleTextAttributes:textAttr forState:state];
    }
    else if ([view isKindOfClass:[UILabel class]])
    {
        [view setValue:[UIColor colorWithHexColor:value] forKey:@"textColor"];
    }
}

@end

#pragma mark - DSRStylishItemKeyAlignment

@implementation DSRStylishAlignmentProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    NSTextAlignment alignment = NSTextAlignmentLeft;

    if ([value isEqualToString:@"left"])
    {
        alignment = NSTextAlignmentLeft;
    }
    else if ([value isEqualToString:@"center"])
    {
        alignment = NSTextAlignmentCenter;
    }
    else if ([value isEqualToString:@"right"])
    {
        alignment = NSTextAlignmentRight;
    }
    else if ([value isEqualToString:@"justified"])
    {
        alignment = NSTextAlignmentJustified;
    }
    else if ([value isEqualToString:@"natural"])
    {
        alignment = NSTextAlignmentNatural;
    }

    if ([view isKindOfClass:[UILabel class]])
    {
        ((UILabel *)view).textAlignment = alignment;
    }
}

@end

#pragma mark - DSRStylishItemKeyText

@implementation DSRStylishTextProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    [view setValue:value forKey:@"text"];
}

@end

#pragma mark - DSRStylishItemKeyFontSize

@implementation DSRStylishFontSizeProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    if ([view isKindOfClass:[UISegmentedControl class]])
    {
        UISegmentedControl *seg = (UISegmentedControl *)view;
        NSMutableDictionary *textAttr = [[seg titleTextAttributesForState:state] mutableCopy];
        UIFont *font = [textAttr objectForKey:NSFontAttributeName];

        if (!textAttr)
        {
            textAttr = [NSMutableDictionary dictionary];
        }

        if (!font)
        {
            font = [UIFont systemFontOfSize:[value floatValue]];
        }
        else
        {
            font = [UIFont fontWithName:font.fontName size:[value floatValue]]; //[font fontName];
        }

        [textAttr setObject:font forKey:NSFontAttributeName];
        [seg setTitleTextAttributes:textAttr forState:state];

        return;
    }

    UIFont *font = [[view valueForKey:@"font"] fontWithSize:[value floatValue]];
    [view setValue:font forKey:@"font"];
}

@end

#pragma mark - DSRStylishItemKeyFontFamily

@implementation DSRStylishFontFamilyProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    if ([view isKindOfClass:[UISegmentedControl class]])
    {
        UISegmentedControl *seg = (UISegmentedControl *)view;
        NSMutableDictionary *textAttr = [[seg titleTextAttributesForState:state] mutableCopy];

        if (!textAttr)
        {
            textAttr = [NSMutableDictionary dictionary];
        }

        UIFont *font = [textAttr objectForKey:NSFontAttributeName];

        CGFloat size = 18;

        if (font)
        {
            size = font.pointSize;
        }

        if ([[value substringToIndex:6] isEqualToString:@"System"])
        {
            NSString *weightString = [[value substringFromIndex:6] lowercaseString];
            CGFloat weight = UIFontWeightRegular;

            if ([weightString isEqualToString:@"-ultralight"])
            {
                weight = UIFontWeightUltraLight;
            }
            else if ([weightString isEqualToString:@"-thin"])
            {
                weight = UIFontWeightThin;
            }
            else if ([weightString isEqualToString:@"-light"])
            {
                weight = UIFontWeightLight;
            }
            else if ([weightString isEqualToString:@"-regular"])
            {
                weight = UIFontWeightRegular;
            }
            else if ([weightString isEqualToString:@"-medium"])
            {
                weight = UIFontWeightMedium;
            }
            else if ([weightString isEqualToString:@"-semibold"])
            {
                weight = UIFontWeightSemibold;
            }
            else if ([weightString isEqualToString:@"-bold"])
            {
                weight = UIFontWeightBold;
            }
            else if ([weightString isEqualToString:@"-heavy"])
            {
                weight = UIFontWeightHeavy;
            }
            else if ([weightString isEqualToString:@"-black"])
            {
                weight = UIFontWeightBlack;
            }

            font = [UIFont systemFontOfSize:size weight:weight];
        }
        else
        {
            font = [UIFont fontWithName:value size:size];
        }

        [textAttr setObject:font forKey:NSFontAttributeName];
        [seg setTitleTextAttributes:textAttr forState:state];

        return;
    }

    UIFont *font = [view valueForKey:@"font"];
    font = [UIFont fontWithName:value size:font.pointSize];
    [view setValue:font forKey:@"font"];
}

@end

@implementation DSRStylishTitlesProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    if ([value isKindOfClass:[NSArray class]] && [view isKindOfClass:[UISegmentedControl class]])
    {
        UISegmentedControl *seg = (UISegmentedControl *)view;
        [seg removeAllSegments];

        for (NSInteger i = [value count] - 1; i >= 0; i--)
        {
            [seg insertSegmentWithTitle:value[i] atIndex:0 animated:YES];
        }
    }
}

@end

#pragma mark - DSRStylishItemKeyDividerColor

@implementation DSRStylishDividerProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    if ([view isKindOfClass:[UISegmentedControl class]])
    {
        UIColor *color = [UIColor colorWithColorString:value];
        UIImage *backgroundImage = [UIColor imageFromColor:color];
        UISegmentedControl *seg = (UISegmentedControl *)view;
        [seg setDividerImage:backgroundImage
            forLeftSegmentState:UIControlStateNormal
              rightSegmentState:UIControlStateNormal
                     barMetrics:UIBarMetricsDefault];
    }
}

@end

#pragma mark - DSRStylishItemKeyTransform

@implementation DSRStylishTransformProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    if ([value isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *parameters = (NSDictionary *)value;
        NSString *type = [parameters objectForKey:@"type"];
        
        if ([type isEqualToString:@"scale"])
        {
            CGFloat xscale = [[parameters objectForKey:@"x"] floatValue];
            CGFloat yscale = [[parameters objectForKey:@"y"] floatValue];
            
            view.transform = CGAffineTransformScale(CGAffineTransformIdentity, xscale, yscale);
        }
    }
}

@end

#pragma mark - DSRStylishItemKeyUnknown

@implementation DSRStylishDefaultProvider

+ (void)applyParameter:(id)value toView:(UIView *)view withState:(UIControlState)state
{
    // Do nothing
}

@end