//
//  UIColor+DSRStylish.h
//  DSRCloudMobileClient
//
//  Created by Zamogilin Andrey on 08.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (DSRStylish)

+ (UIColor *)colorWithColorString:(NSString *)colorString;
+ (UIColor *)colorWithHexColor:(NSString *)hexColor;
+ (UIImage *)imageFromColor:(UIColor *)color;

@end