//
//  NSString+DSRStylish.h
//  DSRCloudMobileClient
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DSRStylish)

+ (NSString *)stringFromStylishString:(NSString *)stylishString;

@end