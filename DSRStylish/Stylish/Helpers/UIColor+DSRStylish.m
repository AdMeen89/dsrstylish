//
//  UIColor+DSRStylish.m
//  DSRCloudMobileClient
//
//  Created by Zamogilin Andrey on 08.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import "UIColor+DSRStylish.h"
#import <HexColors/HexColors.h>

@implementation UIColor (DSRStylish)

+ (UIColor *)colorWithColorString:(NSString *)colorString
{
    return [UIColor colorWithHexColor:colorString];
}

+ (UIColor *)colorWithHexColor:(NSString *)hexColor
{
    return [UIColor hx_colorWithHexRGBAString:hexColor];
}

+ (UIImage *)imageFromColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

#pragma mark - private

@end