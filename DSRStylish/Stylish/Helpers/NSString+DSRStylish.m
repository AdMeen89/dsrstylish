//
//  NSString+DSRStylish.m
//  DSRCloudMobileClient
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import "NSString+DSRStylish.h"

@implementation NSString (DSRStylish)

+ (NSString *)stringFromStylishString:(NSString *)stylishString
{
    if ([[stylishString substringToIndex:1] isEqualToString:@"#"])
    {
        return [stylishString substringFromIndex:1];
    }

    return stylishString;
}

@end