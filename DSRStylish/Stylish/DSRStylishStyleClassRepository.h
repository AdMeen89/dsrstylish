//
//  DSRStylishStyleClassRepository.h
//  DSRCloudMobileClient
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DSRStylishStyleClass;

@interface DSRStylishStyleClassRepository : NSObject

- (DSRStylishStyleClass *)findClassByFullName:(NSString *)name;
- (DSRStylishStyleClass *)findOrCreateWithFullName:(NSString *)name;

@end