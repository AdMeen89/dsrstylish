//
//  DSRStylish.h
//  DSRCloudMobileClient
//
//  Created by Zamogilin Andrey on 08.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DSRStylishStyleClass, DSRStylishItem;

@interface DSRStylish : NSObject

@property (nonatomic) BOOL strongControl;

+ (DSRStylish *)defaultStylish;

- (instancetype)initWithFilename:(NSString *)filename recursive:(BOOL)recursive;
- (void)applyStyleWithItem:(DSRStylishItem *)item forView:(UIView *)view;
- (DSRStylishStyleClass *)loadStyleClassItems:(NSString *)styleClass;

@end