//
//  DSRStylishItem.m
//
//
//  Created by Zamogilin Andrey on 08.04.16.
//
//

#import "DSRStylishItem.h"
#import "DSRStylishAllowableItemsProtocol.h"
#import "DSRStylishProviderProtocol.h"

@interface DSRStylishItem ()

@property (nonatomic, readwrite) DSRStylishItemKey key;
@property (nonatomic, readwrite) DSRStylishItemState state;
@property (nonatomic, copy, readwrite) NSString *value;

@end

@implementation DSRStylishItem

- (DSRStylishItem *)initWithKey:(DSRStylishItemKey)key value:(id)value state:(DSRStylishItemState)state
{
    self = [super init];

    if (self)
    {
        self.state = state;
        self.value = value;
        self.key = key;
    }

    return self;
}

+ (DSRStylishItem *)itemWithKey:(DSRStylishItemKey)key value:(id)value state:(DSRStylishItemState)state
{
    return [[DSRStylishItem alloc] initWithKey:key value:value state:state];
}

+ (BOOL)allowKey:(DSRStylishItemKey)key forClass:(Class)c_class;
{
    if (c_class == nil && key != DSRStylishItemKeyUnknown)
    {
        return YES;
    }

    return [[c_class class] keyIsAllow:key];
}

+ (BOOL)allowState:(DSRStylishItemState)state forClass:(Class)c_class
{
    if (c_class == nil || state == DSRStylishItemStateNormal)
    {
        return YES;
    }

    return [[c_class class] stateIsAllow:state];
}

+ (NSString *)keyStringFromKey:(DSRStylishItemKey)key
{
    switch (key)
    {
    case DSRStylishItemKeyBorderColor:
        return @"border-color";

    case DSRStylishItemKeyBorderWidth:
        return @"border-width";

    case DSRStylishItemKeyBorderCornerRadius:
        return @"border-corner-radius";

    case DSRStylishItemKeyBackgroundColor:
        return @"background-color";

    case DSRStylishItemKeyHidden:
        return @"hidden";
        ;

    case DSRStylishItemKeyOpacity:
        return @"opacity";

    case DSRStylishItemKeyClipsToBounds:
        return @"clips-to-bounds";

    case DSRStylishItemKeyText:
        return @"text";

    case DSRStylishItemKeyColor:
        return @"color";

    case DSRStylishItemKeyAlignment:
        return @"alignment";

    case DSRStylishItemKeyFontSize:
        return @"font-size";

    case DSRStylishItemKeyFontFamily:
        return @"font-family";

    case DSRStylishItemKeyTitles:
        return @"titles";

    case DSRStylishItemKeyDividerColor:
        return @"divider-color";
            
    case DSRStylishItemKeyTransform:
        return @"transform";

    case DSRStylishItemKeyUnknown:
        return nil;
    }
}

+ (DSRStylishItemKey)keyFromString:(NSString *)string
{
    for (NSInteger i = 0; i < DSRStylishItemKeyUnknown; i++)
    {
        NSString *keyString = [self keyStringFromKey:(DSRStylishItemKey)i];

        if ([string isEqualToString:keyString])
        {
            return (DSRStylishItemKey)i;
        }
    }

    return DSRStylishItemKeyUnknown;
}

+ (NSString *)stateStringFromState:(DSRStylishItemState)state
{
    switch (state)
    {
    case DSRStylishItemStateNormal:
        return @"normal";

    case DSRStylishItemStateSelected:
        return @"selected";

    case DSRStylishItemStateDisabled:
        return @"disabled";

    case DSRStylishItemStateHighlighed:
        return @"highlighed";
    }
}

+ (UIControlState)uistateFromState:(DSRStylishItemState)state
{
    switch (state)
    {
    case DSRStylishItemStateNormal:
        return UIControlStateNormal;

    case DSRStylishItemStateDisabled:
        return UIControlStateDisabled;

    case DSRStylishItemStateSelected:
        return UIControlStateSelected;

    case DSRStylishItemStateHighlighed:
        return UIControlStateHighlighted;
    }
}

+ (DSRStylishItemState)stateFromString:(NSString *)string
{
    for (NSInteger i = 0; i < DSRStylishItemStateNormal; i++)
    {
        NSString *keyString = [self stateStringFromState:(DSRStylishItemState)i];

        if ([string isEqualToString:keyString])
        {
            return (DSRStylishItemState)i;
        }
    }

    return DSRStylishItemStateNormal;
}

@end