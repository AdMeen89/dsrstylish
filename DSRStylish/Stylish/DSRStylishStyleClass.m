//
//  DSRStylishStyleClass.m
//  DSRCloudMobileClient
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR Company. All rights reserved.
//

#import "DSRStylishStyleClass.h"
#import "DSRStylishItem.h"

@interface DSRStylishStyleClass ()

@property (copy, nonatomic, readwrite) NSString *fullname;
@property (strong, nonatomic, readwrite) NSMutableArray *items;
@property (strong, nonatomic, readwrite) NSMutableDictionary *states;

@end

@implementation DSRStylishStyleClass

- (DSRStylishStyleClass *)initWithName:(NSString *)name
{
    self = [super init];

    if (self)
    {
        self.items = [NSMutableArray array];
        self.fullname = name;
        self.states = [NSMutableDictionary dictionary];
    }

    return self;
}

- (void)addKey:(DSRStylishItemKey)key withValue:(id)value state:(DSRStylishItemState)state
{
    DSRStylishItem *item = [DSRStylishItem itemWithKey:key value:value state:state];

    NSString *keyString = [DSRStylishItem keyStringFromKey:item.key];
    NSString *stateString = [DSRStylishItem stateStringFromState:state];

    NSMutableDictionary *stateItems = [self.states objectForKey:stateString];

    if (!stateItems)
    {
        stateItems = [NSMutableDictionary dictionary];
    }

    [stateItems setObject:item forKey:keyString];

    [self.states setObject:stateItems forKey:stateString];
    [self.items addObject:item];
}

#pragma mark - getter

- (NSInteger)count
{
    return [self.items count];
}

- (NSArray *)allItems
{
    return self.items;
}

- (NSArray *)allStates
{
    return [self.states allKeys];
}

@end