//
//  DSRStylishViewTests.m
//  DSRStylish
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UIView+DSRStylish.h"

@interface DSRStylishViewTests : XCTestCase

@property (strong, nonatomic) UIView *view;

@end

@implementation DSRStylishViewTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    if (!self.view)
    {
        self.view = [UIView new];
    }
}

- (void)tearDown
{
    self.view = nil;
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// test allowable keys

- (void)testAllowableKeys
{
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyOpacity]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyHidden]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyBorderColor]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyBorderWidth]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyClipsToBounds]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyBackgroundColor]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyBorderCornerRadius]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyUnknown]);
}

- (void)testViewBorderParameters
{
    self.view.layer.borderWidth = 0;
    self.view.layer.cornerRadius = 0;
    self.view.layer.borderColor = [[UIColor clearColor] CGColor];

    self.view.styleClass = @"view-test-class-border";

    UIColor *borderColor = [UIColor colorWithCGColor:self.view.layer.borderColor];

    XCTAssert([borderColor isEqual:[UIColor redColor]]);
    XCTAssert(self.view.layer.borderWidth == 5);
    XCTAssert(self.view.layer.cornerRadius == 10);
}

- (void)testViewBackgroundParameters
{
    self.view.backgroundColor = [UIColor clearColor];
    self.view.styleClass = @"view-test-class-background";
    XCTAssert([self.view.backgroundColor isEqual:[UIColor redColor]]);
}

- (void)testViewOpacityParameters
{
    self.view.alpha = 1.0;
    self.view.styleClass = @"view-test-class-opacity";
    XCTAssert(self.view.alpha == 0.5);
}

- (void)testViewHiddenParameters
{
    self.view.hidden = NO;
    self.view.styleClass = @"view-test-class-hidden";
    XCTAssert(self.view.hidden == YES);
}

@end