//
//  DSRStylishSegmentedControlTests.m
//  DSRStylish
//
//  Created by Andrey on 10.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UIColor+DSRStylish.h"
#import "UISegmentedControl+DSRStylish.h"

@interface DSRStylishSegmentedControlTests : XCTestCase

@property (strong, nonatomic) UISegmentedControl *seg;

@end

@implementation DSRStylishSegmentedControlTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    if (!self.seg)
    {
        self.seg = [[UISegmentedControl alloc] initWithFrame:CGRectZero];
    }
}

- (void)tearDown
{
    self.seg = nil;
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAllowableKeys
{
    XCTAssert([UISegmentedControl keyIsAllow:DSRStylishItemKeyTitles]);
    XCTAssert([UISegmentedControl keyIsAllow:DSRStylishItemKeyFontSize]);
    XCTAssert([UISegmentedControl keyIsAllow:DSRStylishItemKeyFontFamily]);
    XCTAssert([UISegmentedControl keyIsAllow:DSRStylishItemKeyColor]);
}

- (void)testSegmentControlTitlesParameter
{
    [self.seg removeAllSegments];
    self.seg.styleClass = @"seg-test-class-titles";
    XCTAssert([[self.seg titleForSegmentAtIndex:0] isEqualToString:@"item1"]);
    XCTAssert([[self.seg titleForSegmentAtIndex:1] isEqualToString:@"item2"]);
    XCTAssert([[self.seg titleForSegmentAtIndex:2] isEqualToString:@"item3"]);
}

- (void)testSegmentControlDividerColorParameter
{
    self.seg.styleClass = @"seg-test-class-divider";
    UIImage *image1 = [UIColor imageFromColor:[UIColor redColor]];
    UIImage *image2 = [self.seg dividerImageForLeftSegmentState:UIControlStateNormal
                                              rightSegmentState:UIControlStateNormal
                                                     barMetrics:UIBarMetricsDefault];

    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);

    XCTAssert([data1 isEqual:data2]);
}

- (void)testSegmentControlFontParameter
{
    [self.seg removeAllSegments];
    self.seg.styleClass = @"seg-test-class-font";

    NSDictionary *textAttrNormal = [self.seg titleTextAttributesForState:UIControlStateNormal];
    NSDictionary *textAttrSelected = [self.seg titleTextAttributesForState:UIControlStateSelected];
    NSDictionary *textAttrHighlighed = [self.seg titleTextAttributesForState:UIControlStateHighlighted];
    NSDictionary *textAttrDisabled = [self.seg titleTextAttributesForState:UIControlStateDisabled];

    UIFont *normalFont = [textAttrNormal objectForKey:NSFontAttributeName];
    UIFont *selectedFont = [textAttrSelected objectForKey:NSFontAttributeName];
    UIFont *highlighedFont = [textAttrHighlighed objectForKey:NSFontAttributeName];
    UIFont *disabledFont = [textAttrDisabled objectForKey:NSFontAttributeName];

    UIFont *normalFontVerify = [UIFont systemFontOfSize:10 weight:UIFontWeightRegular];
    UIFont *selectedFontVerify = [UIFont systemFontOfSize:19 weight:UIFontWeightBold];
    UIFont *highlighedFontVerify = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
    UIFont *disabledFontVerify = [UIFont fontWithName:@"HelveticaNeue-Bold" size:20];

    XCTAssert([normalFont.fontName isEqualToString:normalFontVerify.fontName] &&
              [normalFont.familyName isEqualToString:normalFontVerify.familyName] && normalFont.pointSize == normalFontVerify.pointSize);

    XCTAssert([selectedFont.fontName isEqualToString:selectedFontVerify.fontName] &&
              [selectedFont.familyName isEqualToString:selectedFontVerify.familyName]
              && selectedFont.pointSize == selectedFontVerify.pointSize);

    XCTAssert([highlighedFont.fontName isEqualToString:highlighedFontVerify.fontName] &&
              [highlighedFont.familyName isEqualToString:highlighedFontVerify.familyName]
              && highlighedFont.pointSize == highlighedFontVerify.pointSize);

    XCTAssert([disabledFont.fontName isEqualToString:disabledFontVerify.fontName] &&
              [disabledFont.familyName isEqualToString:disabledFontVerify.familyName]
              && disabledFont.pointSize == disabledFontVerify.pointSize);
}

@end