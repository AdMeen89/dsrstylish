//
//  DSRStylishLabelTests.m
//  DSRStylish
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <DSRStylish/UILabel+DSRStylish.h>
#import "DSRStylishItem.h"

@interface DSRStylishLabelTests : XCTestCase

@property (strong, nonatomic) UILabel *label;

@end

@implementation DSRStylishLabelTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    if (!self.label)
    {
        self.label = [[UILabel alloc] initWithFrame:CGRectZero];
    }
}

- (void)tearDown
{
    self.label = nil;
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// test allowable keys

- (void)testAllowableKeys
{
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyText]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyFontSize]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyFontFamily]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyAlignment]);
    XCTAssert([UILabel keyIsAllow:DSRStylishItemKeyColor]);
}

- (void)testLabelTextParameter
{
    self.label.text = @"";
    self.label.styleClass = @"label-test-class-text";
    XCTAssert([self.label.text isEqualToString:@"test-text"]);
}

- (void)testLabelColorParameter
{
    self.label.textColor = [UIColor clearColor];
    self.label.styleClass = @"label-test-class-color";
    XCTAssert([self.label.textColor isEqual:[UIColor redColor]]);
}

- (void)testLabelFontParameters
{
    self.label.font = [UIFont fontWithName:@"Arial" size:22];
    self.label.styleClass = @"label-test-class-font";
    XCTAssert([self.label.font isEqual:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.5]]);
}

- (void)testLabelAlignmentBadValueParameters
{
    self.label.textAlignment = NSTextAlignmentRight;
    self.label.styleClass = @"label-test-class-alignment.default";
    XCTAssert(self.label.textAlignment == NSTextAlignmentLeft);
}

- (void)testLabelAlignmentCorrectValueParameters
{
    self.label.textAlignment = NSTextAlignmentRight;
    self.label.styleClass = @"label-test-class-alignment.left";
    XCTAssert(self.label.textAlignment == NSTextAlignmentLeft);

    self.label.styleClass = @"label-test-class-alignment.right";
    XCTAssert(self.label.textAlignment == NSTextAlignmentRight);

    self.label.styleClass = @"label-test-class-alignment.center";
    XCTAssert(self.label.textAlignment == NSTextAlignmentCenter);

    self.label.styleClass = @"label-test-class-alignment.justified";
    XCTAssert(self.label.textAlignment == NSTextAlignmentJustified);

    self.label.styleClass = @"label-test-class-alignment.natural";
    XCTAssert(self.label.textAlignment == NSTextAlignmentNatural);
}

@end