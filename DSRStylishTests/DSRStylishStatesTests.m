//
//  DSRStylishStatesTests.m
//  DSRStylish
//
//  Created by Andrey on 09.04.16.
//  Copyright © 2016 DSR. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DSRStylishStyleClass.h"
#import "DSRStylishItem.h"
#import "DSRStylish.h"

@interface DSRStylishStatesTests : XCTestCase

@end

@implementation DSRStylishStatesTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
// first, allways shoud be normal state
// second, all states should be
- (void)testStateExistsNormalState
{
    DSRStylishStyleClass *class = [[DSRStylish defaultStylish] loadStyleClassItems:@"test-class-states"];
    BOOL isFind = NO;
    for (NSString *state in class.allStates)
    {
        isFind = [state isEqualToString:@"normal"];

        if (isFind)
        {
            break;
        }
    }

    XCTAssert(isFind);
}

- (void)testStateLoadCorrectCountState
{
    DSRStylishStyleClass *class = [[DSRStylish defaultStylish] loadStyleClassItems:@"test-class-states"];
    NSArray *states = [class allStates];
    XCTAssert([states count] == 3);
}

@end